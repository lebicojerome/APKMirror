package taco.apkmirror.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.MITLicense;
import de.psdev.licensesdialog.model.Notice;
import de.psdev.licensesdialog.model.Notices;
import taco.apkmirror.R;

public class PreferencesFragment extends PreferenceFragment {
    SharedPreferences prefsFragment;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        prefsFragment = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        Preference gitlab = findPreference("gitlab");
        Preference libs = findPreference("libs");
        Preference xda = findPreference("xda");

        gitlab.setOnPreferenceClickListener(preference -> {
            Intent gitlabIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/TacoTheDank/APKMirror"));
            startActivity(gitlabIntent);

            return true;
        });

        libs.setOnPreferenceClickListener(preference -> {
            final Notices n = new Notices();
            n.addNotice(new Notice("jsoup: Java HTML Parser", "https://github.com/jhy/jsoup", "Copyright (c) 2009-2018 Jonathan Hedley <jonathan@hedley.net>", new MITLicense()));
            n.addNotice(new Notice("AdvancedWebView", "https://github.com/delight-im/Android-AdvancedWebView", "Copyright (c) delight.im (https://www.delight.im/)", new MITLicense()));
            n.addNotice(new Notice("BottomBar library for Android", "https://github.com/roughike/BottomBar", "Copyright (c) 2016 Iiro Krankka (https://github.com/roughike)", new ApacheSoftwareLicense20()));
            n.addNotice(new Notice("Material Dialogs", "https://github.com/afollestad/material-dialogs", "Copyright (c) 2014-2016 Aidan Michael Follestad", new MITLicense()));
            n.addNotice(new Notice("LicensesDialog", "https://github.com/PSDev/LicensesDialog", "Copyright 2013-2017 Philip Schiffer", new ApacheSoftwareLicense20()));

            new LicensesDialog.Builder(getActivity())
                    .setNotices(n)
                    .setTitle(getString(R.string.libraries))
                    .build()
                    .show();
            return true;
        });

        xda.setOnPreferenceClickListener(preference -> {
            Intent threadIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://forum.xda-developers.com/android/apps-games/apkmirror-web-app-t3450564"));
            startActivity(threadIntent);
            return true;
        });
    }
}
